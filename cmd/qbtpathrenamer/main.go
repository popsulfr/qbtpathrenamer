// MIT License
// 
// Copyright (c) 2020 Philipp Richter
// 
// Permission is hereby granted, free of charge, to any person obtaining a copy
// of this software and associated documentation files (the "Software"), to deal
// in the Software without restriction, including without limitation the rights
// to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
// copies of the Software, and to permit persons to whom the Software is
// furnished to do so, subject to the following conditions:
// 
// The above copyright notice and this permission notice shall be included in all
// copies or substantial portions of the Software.
// 
// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
// IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
// FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
// AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
// LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
// OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
// SOFTWARE.

package main

import (
	"bufio"
	"encoding/json"
	"errors"
	"flag"
	"fmt"
	"io"
	"log"
	"os"
	"path/filepath"
	"runtime"
	"strings"
	"sync"

	"github.com/adrg/xdg"
	"github.com/zeebo/bencode"
)

var stdErrLog = log.New(os.Stderr, "", log.LstdFlags|log.Lshortfile)
var stdOutLog = log.New(os.Stdout, "", log.LstdFlags)

var QBTLocalSubdirs = []string{"qBittorrent", "BT_backup"}
var QBTBackupPathDefault string
var FastresumeFields = []string{"qBt-savePath", "save_path"}
var PathSeparatorDefault string

const filesAtOnce = 1024

// Config ::
type Config struct {
	QBTBackupPath string
	PathSeparator string
	PathMappings  map[string]string
	ForceRead     bool
}

func init() {
	if runtime.GOOS == "linux" {
		QBTBackupPathDefault = filepath.Join(append([]string{xdg.DataHome, "data"}, QBTLocalSubdirs...)...)
	} else {
		QBTBackupPathDefault = filepath.Join(append([]string{xdg.DataHome}, QBTLocalSubdirs...)...)
	}
	if runtime.GOOS == "windows" {
		PathSeparatorDefault = ";"
	} else {
		PathSeparatorDefault = ":"
	}
}

func readFastresume(filePath string, force bool) (map[string]interface{}, error) {
	file, err := os.OpenFile(filePath, os.O_RDONLY, 0)
	if err != nil {
		return nil, err
	}
	defer file.Close()
	bdec := bencode.NewDecoder(bufio.NewReader(file))
	valMap := map[string]interface{}{}
	if err := bdec.Decode(&valMap); err != nil {
		if force && len(valMap) > 0 {
			stdErrLog.Printf("WARNING forced read for '%s' : %s\n", filePath, err)
			return valMap, nil
		}
		return nil, err
	}
	return valMap, nil
}

func writeFastresume(filePath string, valMap interface{}) error {
	file, err := os.OpenFile(filePath, os.O_WRONLY|os.O_TRUNC, 0666)
	if err != nil {
		return err
	}
	defer file.Close()
	bufFile := bufio.NewWriter(file)
	defer bufFile.Flush()
	benc := bencode.NewEncoder(bufFile)
	return benc.Encode(valMap)
}

func replaceSavePath(valMap map[string]interface{}, pathMappings map[string]string) bool {
	var replaced bool
	for _, field := range FastresumeFields {
		val, ok := valMap[field]
		if !ok {
			continue
		}
		valStr, ok := val.(string)
		if !ok {
			continue
		}
		for src, repl := range pathMappings {
			if strings.HasPrefix(valStr, src) {
				valMap[field] = repl + valStr[len(src):]
				replaced = true
				break
			}
		}
	}
	return replaced
}

func handleFastresume(conf *Config, filePath string) error {
	valMap, err := readFastresume(filePath, conf.ForceRead)
	if err != nil {
		stdErrLog.Printf("Decoding error '%s' : %s\n", filePath, err)
		return err
	}
	if !replaceSavePath(valMap, conf.PathMappings) {
		return nil
	}
	err = writeFastresume(filePath, valMap)
	if err != nil {
		stdErrLog.Printf("Error writing to '%s' : %s\n", filePath, err)
		return err
	}
	stdOutLog.Printf("Written to '%s'\n", filePath)
	return nil
}

func configFromCmdline(conf *Config) error {
	flagSet := flag.NewFlagSet(os.Args[0], flag.ContinueOnError)
	flagSet.StringVar(&conf.QBTBackupPath, "qbtbackuppath", QBTBackupPathDefault, "BT_backup path or path where *.fastresume files are stored")
	flagSet.StringVar(&conf.PathSeparator, "pathseparator", PathSeparatorDefault, "The path separator to use for parsing the mapping")
	flagSet.BoolVar(&conf.ForceRead, "forceread", false, "Force read even on files with invalid input")
	var pathMappingsStr string
	flagSet.StringVar(&pathMappingsStr, "pathmappings", "", fmt.Sprintf("A mapping looks like '/source/path1%s/replaced/path1%s/source/path2%s/replaced/path2...'", PathSeparatorDefault, PathSeparatorDefault, PathSeparatorDefault))
	err := flagSet.Parse(os.Args[1:])
	if err != nil {
		return err
	}
	conf.QBTBackupPath = strings.TrimSpace(conf.QBTBackupPath)
	if conf.QBTBackupPath == "" {
		return errors.New("-qbtbackuppath can not be empty")
	}
	conf.PathSeparator = strings.TrimSpace(conf.PathSeparator)
	if conf.PathSeparator == "" {
		return errors.New("-pathseparator can not be empty")
	}
	pathMappingsStr = strings.TrimSpace(pathMappingsStr)
	if pathMappingsStr == "" {
		return errors.New("-pathmappings can not be empty")
	}
	paths := strings.Split(pathMappingsStr, conf.PathSeparator)
	conf.PathMappings = make(map[string]string, len(paths)/2)
	for i := 0; i < len(paths); i += 2 {
		pathSrc := strings.TrimSpace(paths[i])
		if pathSrc == "" {
			continue
		}
		pathRepl := ""
		if i+1 < len(paths) {
			pathRepl = strings.TrimSpace(paths[i+1])
		}
		conf.PathMappings[pathSrc] = pathRepl
	}
	if len(conf.PathMappings) == 0 {
		return errors.New("-pathmappings has no viable sources for replacement")
	}
	return nil
}

func main() {
	exitCode := 1
	defer func() { os.Exit(exitCode) }()

	var conf Config
	if err := configFromCmdline(&conf); err != nil {
		if errors.Is(err, flag.ErrHelp) {
			exitCode = 0
			return
		}
		stdErrLog.Println(err)
		return
	}
	stdOutLog.Println("Configuration :")
	jenc := json.NewEncoder(stdOutLog.Writer())
	jenc.SetIndent("", "  ")
	jenc.Encode(conf)
	fi, err := os.Stat(conf.QBTBackupPath)
	if err != nil {
		stdErrLog.Println(err)
		return
	}
	if fi.IsDir() {
		dir, err := os.OpenFile(conf.QBTBackupPath, os.O_RDONLY, 0)
		if err != nil {
			stdErrLog.Println(err)
			return
		}
		defer dir.Close()
		numCpus := runtime.NumCPU()
		closeChan := make(chan bool)
		fileChan := make(chan string)
		wg := sync.WaitGroup{}
		wg.Add(numCpus)
		for i := 0; i < numCpus; i++ {
			go func(i int, fileChan <-chan string, closeChan <-chan bool) {
				defer wg.Done()
				for {
					select {
					case filePath := <-fileChan:
						err := handleFastresume(&conf, filePath)
						if err != nil {
							exitCode = 2
						}
					case <-closeChan:
						return
					}
				}
			}(i, fileChan, closeChan)
		}

		for {
			files, err := dir.Readdirnames(filesAtOnce)
			if err != nil {
				if !errors.Is(err, io.EOF) {
					stdErrLog.Println(err)
					exitCode = 2
				}
				break
			}
			for _, file := range files {
				if filepath.Ext(file) == ".fastresume" {
					fileChan <- filepath.Join(conf.QBTBackupPath, file)
				}
			}
		}
		close(closeChan)
		wg.Wait()
		if exitCode < 2 {
			exitCode = 0
		}
	} else {
		err := handleFastresume(&conf, conf.QBTBackupPath)
		if err != nil {
			exitCode = 2
		} else {
			exitCode = 0
		}
	}
}
