# qBTPathRenamer

Tool for [qBittorrent](https://www.qbittorrent.org/) to rename save paths for torrents directly in the `*.fastresume` files.
For when torrents are moved to another platform or setup, save paths can simply be renamed from something like `/src/path/` to `/new/path/`.
It's multithreaded and very fast even on huge torrent libraries.

The `.fastresume` files are usually stored in :

- Linux : `~/.local/share/data/qBittorrent/BT_backup`
- Windows : `%LOCALAPPDATA%\qBittorrent\BT_backup`
- Mac OS : `~/Library/Application Support/qBittorrent/BT_backup`

See : [Where does qBittorrent save its settings?](https://github.com/qbittorrent/qBittorrent/wiki/Frequently-Asked-Questions#Where_does_qBittorrent_save_its_settings)

## Build

```
go build -ldflags "-s -w" ./cmd/qbtpathrename
```

## Usage

```
./qbtpathrenamer -help
Usage of ./qbtpathrenamer:
  -forceread
        Force read even on files with invalid input
  -pathmappings string
        A mapping looks like '/source/path1:/replaced/path1:/source/path2:/replaced/path2...'
  -pathseparator string
        The path separator to use for parsing the mapping (default ":")
  -qbtbackuppath string
        BT_backup path or path where *.fastresume files are stored (default "/home/popsulfr/.local/share/data/qBittorrent/BT_backup")
```

* **STRONGLY RECOMMEND MAKING A BACKUP OF THE FILES BEFOREHAND**
* The default separator on Windows is `;` while on other platforms it is `:`.
  - For renaming save paths in Linux from Windows sources it might be a good idea to force another `-pathseparator` (e.g.: `C:\my\src\;/my/new/dest/`).
* `-pathmappings` is required and represents pairs of `source:replacement` path strings to apply to the save paths.
* It should be able to detect the correct folder otherwise specify it with `-qbtbackuppath`.
* In some cases while decoding invalid types might be encountered in the `.fastresume` files (they will be printed). If you still want to go ahead and use what could be read, specify `-forceread`. The rewritten files won't have the unreadable content.
* The mapping source match is always from the beginning of the path. So to rename for instance `/media/Downloads/` to `/media/Newdownloads/` you **CAN NOT** do `-pathmappings '/Downloads/:/Newdownloads/'` it has to be `-pathmappings '/media/Downloads/:/media/Newdownloads/'`.

Example Windows to Linux save paths :

```
./qbtpathrenamer -pathseparator ';' -pathmappings 'C:\\my\\windows\\download\\folder\\;/my/unix/download/folder/'
```
